#!/bin/bash
option=$1


while getopts 'lb:d:' OPTION; do
  case "$OPTION" in
    l)
      git branch
      ;;
    b)
      branch_name="$OPTARG"
      echo "The value provided is $OPTARG"
      git branch $branch_name
      ;;
    d)
      branch_name="$OPTARG"
      echo "The value provided is $OPTARG"
      git branch --delete $branch_name
      ;;
    r)
      avalue="$OPTARG"
      echo "The value provided is $OPTARG"
      ;;
    #?)
    #  echo "script usage: $(basename \$0) [-l] [-h] [-a somevalue]" >&2
     # exit 1
     # ;;
  esac
done

case $option in

"-m")
  branch_name1=$2
  branch_name2=$3
  
  git checkout $branch_name2
  git merge $branch_name1 

     
;;
"-r")
  branch_name1=$2
  branch_name2=$3

  git checkout $branch_name1
  git rebase $branch_name2
  #git checkout $branch_name2
  #git rebase $branch_name1
 
;;
  
esac

